(setq auto-save-list-file-prefix ; Prefix for generating auto-save-list-file-name
  (expand-file-name ".auto-save-list/.saves-" user-emacs-directory)
  auto-save-default t        ; Auto-save every buffer that visits a file
  auto-save-timeout 20       ; Number of seconds between auto-save
  auto-save-interval 200)    ; Number of keystrokes between auto-saves

(message "save")

(setq backup-directory-alist       ; File name patterns and backup directory names.
      `(("." . ,(expand-file-name "backups" user-emacs-directory)))
      make-backup-files t          ; Backup of a file the first time it is saved.
      vc-make-backup-files nil     ; No backup of files under version contr
      backup-by-copying t          ; Don't clobber symlinks
      version-control t            ; Version numbers for backup file
      delete-old-versions t        ; Delete excess backup files silently
      kept-old-versions 6          ; Number of old versions to keep
      kept-new-versions 9          ; Number of new versions to keep
      delete-by-moving-to-trash t) ; Delete files to trash

(message "backup")
