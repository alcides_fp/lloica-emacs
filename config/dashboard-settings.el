;; load recent files

(require 'recentf)
(recentf-mode 1)
(setq recentf-max-menu-items 25)
(setq recentf-max-saved-items 25)
(global-set-key "\C-x\ \C-r" 'recentf-open-files)


;; registro de  historial
(require 'savehist)
(setq kill-ring-max 50
      history-length 50)
(setq savehist-additional-variables
      '(kill-ring
        command-history
        set-variable-value-history
        custom-variable-history   
        query-replace-history     
        read-expression-history   
        minibuffer-history        
        read-char-history         
        face-name-history         
        bookmark-history
        file-name-history))
 (put 'minibuffer-history         'history-length 50)
 (put 'file-name-history          'history-length 50)
 (put 'set-variable-value-history 'history-length 25)
 (put 'custom-variable-history    'history-length 25)
 (put 'query-replace-history      'history-length 25)
 (put 'read-expression-history    'history-length 25)
 (put 'read-char-history          'history-length 25)
 (put 'face-name-history          'history-length 25)
 (put 'bookmark-history           'history-length 25)
(setq history-delete-duplicates t)
(let (message-log-max)
  (savehist-mode))

;; posicion del cursor

(setq save-place-file (expand-file-name "saveplace" user-emacs-directory)
      save-place-forget-unreadable-files t)
(save-place-mode 1)


;; Tablero de inicio

(use-package dashboard
  :preface
    (setq dashboard-banner-logo-title (concat "GNU Lloica Emacs " emacs-version
					    " kernel " (car (split-string (shell-command-to-string "uname -r") "-"))
					    " x86_64 " (car (split-string (shell-command-to-string "/usr/bin/sh -c '. /etc/os-release && echo $PRETTY_NAME'") "\n"))))
  :custom (dashboard-set-file-icons t)
  :custom (dashboard-startup-banner 'logo)
  :config (dashboard-setup-startup-hook))
