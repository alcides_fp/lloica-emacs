(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 6))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(setq package-enable-at-startup nil)

;; usar use-package con straight
(message "enable use-package with straight")
(straight-use-package 'use-package)
;; Configure use-package to use straight.el by default
(use-package straight
  :custom (straight-use-package-by-default nil))


(unless package-archive-contents
  (package-refresh-contents))
(package-install-selected-packages)

(defun ensure-package-installed (&rest packages)
 "Assure every package is installed, ask for installation if it's not"
 (mapcar
    (lambda (package)
      ;; (package-installed-p 'evil)
      (if (package-installed-p package)
	  nil
	(if (y-or-n-p (format "Package %s is missing. Install it? " package))
	    (package-install package)
	  package)))
    packages))

(or (file-exists-p package-user-dir)
    (package-refresh-contents))

(ensure-package-installed 'iedit 'magit)

(message "enable base packages")

(setq package-list
      '(cape                ; Completion At Point Extensions
        orderless           ; Completion style for matching regexps in any order
        vertico             ; VERTical Interactive COmpletion
        marginalia          ; Enrich existing commands with completion annotations
        consult             ; Consulting completing-read
        corfu               ; Completion Overlay Region FUnction
        deft                ; Quickly browse, filter, and edit plain text notes
        elfeed              ; Emacs Atom/RSS feed reader
        elfeed-org          ; Configure elfeed with one or more org-mode files
        citar               ; Citation-related commands for org, latex, markdown
        ;;citeproc            ; A CSL 1.0.2 Citation Processor
        flyspell-correct-popup ; Correcting words with flyspell via popup interface
        flyspell-popup      ; Correcting words with Flyspell in popup menus
        guess-language      ; Robust automatic language detection
        helpful             ; A better help buffer
        mini-frame          ; Show minibuffer in child frame on read-from-minibuffer
        ;imenu-list          ; Show imenu entries in a separate buffer
        ;;magit               ; A Git porcelain inside Emacs.
        markdown-mode       ; Major mode for Markdown-formatted text
        multi-term          ; Managing multiple terminal buffers in Emacs.
        pinentry            ; GnuPG Pinentry server implementation
        use-package         ; A configuration macro for simplifying your .emacs
        which-key
	org
	company
	neotree
	nlinum
	rainbow-delimiters
	unicode-escape
	font-utils
	fontify-face
	fontawesome
	unicode-input
	;;unicode-fonts
	))         ; Display available keybindings in popup


(message "activate base packages")
(dolist (package package-list)
  (straight-use-package package))
