;; Configuración para herramientas asesoras
;; usabilidad de tabs y espacios

(setq-default tab-width 4)
(setq tab-width 4)
(setq-default tab-always-indent t)

;; configuración de company
(require 'company)
(add-hook 'after-init-hook 'global-company-mode)
(setq company-idle-delay 0)
(setq company-show-numbers t)
;; (setq lsp-enable-links nil)
(straight-use-package 'company-tabnine :ensure t)
(add-to-list 'company-backends #'company-tabnine)

;; Trigger completion immediately.
(setq company-idle-delay 0)

;; Number the candidates (use M-1com, M-2 etc to select completions).
(setq company-show-numbers t)

;; se añade tabnine a backends de company
;;(use-package 'company-tabnine :ensure t)
;;(add-to-list 'company-backends #'tabnine)


;; gestion de directorios
;;(use-package 'sr-speedbar)
(message "Enable sr speedbar")
(straight-use-package 'helm)
(straight-use-package 'sr-speedbar)
(setq sr-speedbar-right-side nil)
(straight-use-package 'speedbar-git-respect)
(speedbar-git-respect-mode t)
(setq byte-compile-warnings '(not free-vars ))


(message "Enable neotree")
;; activacion neotree
(straight-use-package 'neotree)
(global-set-key [f8] 'neotree-toggle)


;; activacion modo electrico para parentesis
;; se instala electric-{cursor,spacing}
(electric-pair-mode 1)



(message "Enable nlinum")
;; activación numero de linea
(straight-use-package 'nlinum-hl)

;; (defun my-nlinum-mode-hook ()
;;   (when nlinum-mode
;;     (setq-local nlinum-format
;;                 (concat "%" (number-to-string
;;                              ;; Guesstimate number of buffer lines.
;;                              (ceiling (log (max 1 (/ (buffer-size) 80)) 10)))
;;                         "d"))))

;; (add-hook 'nlinum-mode-hook #'my-nlinum-mode-hook)
(global-nlinum-mode)



(message "Activate rainbow-delimiters")
;; rainbow delimiters
(straight-use-package 'rainbow-delimiters)
(add-hook 'prog-mode-hook 'rainbow-delimiters-mode)


;; vertico
(message "Enable vertico")
(straight-use-package 'vertico-posframe)
(vertico-posframe-mode 1)
(setq vertico-posframe-parameters
      '((left-fringe . 8)
        (right-fringe . 8)))


;; Optionally use the `orderless' completion style.
(message "Enable orderless")

(use-package orderless
  :ensure t
  :custom
  (completion-styles '(orderless basic))
  (completion-category-overrides '((file (styles basic partial-completion)))))


;; Marginalia
(message "Enable marginalia")

(use-package marginalia
  ;; Either bind `marginalia-cycle` globally or only in the minibuffer
  :bind (("M-A" . marginalia-cycle)
         :map minibuffer-local-map
         ("M-A" . marginalia-cycle))

  ;; The :init configuration is always executed (Not lazy!)
  :init

  ;; Must be in the :init section of use-package such that the mode gets
  ;; enabled right away. Note that this forces loading the package.
  (marginalia-mode))


;; Selectrium autocompletado
;; gestor para filtrado
(message "Enable selectrum")
(straight-use-package 'selectrum)
(straight-use-package 'prescient)
(straight-use-package 'ivy-prescient)
(straight-use-package 'company-prescient)
(straight-use-package 'selectrum-prescient)
(selectrum-mode +1)
(selectrum-prescient-mode +1)
(setq completion-styles '(prescient basic))

;; fuentes unicode

(require 'unicode-fonts)
(unicode-fonts-setup)


;; snippets preconfigurados yasnippets
(straight-use-package 'yasnippets)
(straight-use-package 'auto-yasnippet)

;;(use-package company-tabnine :ensure t)
;;(add-to-list 'company-backends #'company-tabnine)

