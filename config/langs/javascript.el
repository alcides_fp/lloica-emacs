;; javascript
(straight-use-package 'javascript)
(straight-use-package 'js2-mode)
(straight-use-package 'js2-closure)
(straight-use-package 'js2-refactor)

;;(straight-use-package 'discover-js2-refactor)
(straight-use-package 'xref-js2)

(require 'js2-mode)
(add-to-list 'auto-mode-alist '("\\.js\\'" . js2-mode))

;; ;; Better imenu
(add-hook 'js2-mode-hook #'js2-imenu-extras-mode)


(require 'js2-refactor)
(require 'xref-js2)
(add-hook 'js2-mode-hook #'js2-refactor-mode)
(js2r-add-keybindings-with-prefix "C-c C-r")

;; (define-key js2-mode-map (kbd "C-k") #'js2r-kill)

;; ;; js-mode (which js2 is based on) binds "M-." which conflicts with xref, so
;; ;; unbind it.
;; (define-key js-mode-map (kbd "M-.") nil)

;; (add-hook 'js2-mode-hook (lambda ()
;; 						   (add-hook 'xref-backend-functions #'xref-js2-xref-backend nil t)))


;; (define-key js2-mode-map (kbd "C-k") #'js2r-kill)
;; ;; typescript
;; (straight-use-package 'typescript-mode)

;; ;; vue framework configuration
;; (straight-use-package 'vue-mode)
;; (straight-use-package 'vue-html-mode)

;; ;;(add-to-list 'eglot-server-programs '(web-mode "vls"))
;; (add-to-list 'load-path "~/.emacs.d/extras/vue-mode")
;; (load "~/.emacs.d/extras/vue-mode/vue-mode.el")
;; (require 'vue-mode)

;; (setq vue-mode-packages
;; 	  '(vue-mode))

;; (setq vue-mode-excluded-packages '())

;; (defun vue-mode/init-vue-mode ()
;;   "Initialize my package"
;;   (use-package vue-mode
;; 	:mode "\\.vue\\'"
;; 	:hook (vue-mode . prettier-js-mode)
;; 	:config
;; 	(add-hook 'vue-mode-hook #'lsp)
;; 	(setq prettier-js-args '("--parser vue")))
;;   )
