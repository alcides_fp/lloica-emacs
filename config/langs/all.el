;; https://tree-sitter.github.io/tree-sitter/

(straight-use-package 'tree-sitter)

(global-tree-sitter-mode)

;; load language settings

;; rust
;; c c++
;; python
;; haskell
;; javascript

(setq files '(
			  "python.el"
			  "javascript.el"
			  "haskell.el"
			  "rust.el"
			  "c_c++.el"
			  "markdown.el"
			  "common_lisp.el"
			  ))

(defun load-config (file)
  (message (concat "Cargando" file))
  (load file)
)

(defun load-files (files) "Leer archivos desde lista"
(let (value) 
  (dolist 
	(file files value) 
	(load-config (expand-file-name file (concat user-emacs-directory "config/langs/"))
		  )
	)
  )
)

(load-files files)

;; awk
(straight-use-package 'awk-it)
