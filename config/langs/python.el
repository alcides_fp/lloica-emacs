(setq python-python-command "/usr/bin/env python3")
(setq python-shell-interpreter "/usr/bin/env python3")

(setq
 python-shell-interpreter "ipython"
 python-shell-interpreter-args "-i")

(straight-use-package 'virtualenvwrapper)
;;(straight-use-package 'auto-virtualenvwrapper)

(venv-initialize-interactive-shells) ;; if you want interactive shell support
(venv-initialize-eshell) ;; if you want eshell support

;; note that setting `venv-location` is not necessary if you
;; use the default location (`~/.virtualenvs`), or if the
;; the environment variable `WORKON_HOME` points to the right place

(setq lsp-python-ms-python-executable "python3")
(setq lsp-python-ms-extra-paths "/usr/bin/env python3")

(setq venv-location "~/.virtualenvs")

(straight-use-package 'flycheck)

(defun flycheck-python-setup ()
  (flycheck-mode))
(add-hook 'python-mode-hook #'flycheck-python-setup)

;;(flycheck-add-next-checker 'python-flake8 'python-pylint 'python-mypy)

