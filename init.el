;; check for version
(when (version< emacs-version "28.1") (error "Necesitas tener instalado emacs 28.1 o mas"))

(add-to-list 'load-path "~/.emacs.d/config/")


;; se define archivo de inicializacion
(setq base-file (expand-file-name "base.el" user-emacs-directory))


;; si el archivo existe se carga 
(when (file-exists-p base-file)
  (load base-file)
  )
