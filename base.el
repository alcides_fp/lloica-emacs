(require 'loadhist)
(file-dependents (feature-file 'cl))

(require 'package)

(setq package-archives
      '(("melpa stable" . "http://stable.melpa.org/packages/")
	("gnu" . "http://elpa.gnu.org/packages/")))

(message "loading package manager")

;; package manager straight
(setq pm-file
      (expand-file-name "config/package-manager.el" user-emacs-directory))
(when (file-exists-p pm-file) (load pm-file))


;; codification utf-8
(message "activate utf-8")
(set-default-coding-systems 'utf-8)

;; ;; autosave
(message "activate backup file configuration")

;; ;; se define archivo de inicializacion
(setq bas-file
      (expand-file-name "config/backup-autosave.el" user-emacs-directory))

;; ;; si el archivo existe se carga 
(when (file-exists-p bas-file)
  (load bas-file)
  )

;; ;;(when (file-exists-p dashboard-file) (org-babel-load-file dashboard-file))

;; ;; ;; theme and icons
(message "Load theming configuration")
(setq theming-file
      (expand-file-name "config/theming.el" user-emacs-directory))

(message theming-file)
(when (file-exists-p theming-file) (load theming-file))


;; ;; dashboard
(message "Enable dashboard")
(setq dashboard-file
      (expand-file-name "config/dashboard-settings.el" user-emacs-directory))
;; si el archivo existe se carga 
(when (file-exists-p dashboard-file)
  (load dashboard-file)
  )


;; ;; herramientas para asesorar el desarrollo
(message "Load asesor configuration")
(setq asesor-file
     (expand-file-name "config/asesor.el" user-emacs-directory))

(message asesor-file)
(when (file-exists-p asesor-file) (load asesor-file))


;; ;; configuracion orgmode

(message "Load orgmode configuration")
(setq orgmode-file
      (expand-file-name "config/orgmode.el" user-emacs-directory))

(message orgmode-file)
(when (file-exists-p orgmode-file) (load orgmode-file))


;; configuracion all languages

(message "Load orgmode configuration")
(setq langs-file
     (expand-file-name "config/langs/all.el" user-emacs-directory))

(message langs-file)
(when (file-exists-p langs-file) (load langs-file))
